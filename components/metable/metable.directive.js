app.directive('metable', function() {
    return {
        restrict: 'E',
        transclude: true,

        templateUrl: 'components/metable/metable.html'
    };
});
