app.factory('services', function($http, $localStorage) {
    var serviceBase = 'data/'
    var obj = {};

    obj.add = function(name, data) {
        return new Promise(function(resolve, reject) {
            setTimeout(function() {
                data[name + '_id'] = Math.floor((Math.random() * 10000) + 1);
                if (!$localStorage[name]) {
                    $localStorage[name] = [];
                }
                $localStorage[name].push(data);
                resolve(data);
            }, 1500);
        });
    }

    obj.get = function(name) {
        var sliced = Array.prototype.slice.call(arguments);
        var result = {};
        var promises = sliced.map(function(item) {
            return new Promise(function(resolve, reject) {
                var ret = [];
                angular.copy($localStorage[item], ret);
                result[item] = ret;
                resolve(ret);
            });
        });
        return Promise.all(promises).then(function() {
            return result;
        });
    }

    obj.update = function(name, data) {
        return new Promise(function(resolve, reject) {
            setTimeout(function() {
                var el = $localStorage[name]
                var query = {};
                query[name + '_id'] = data[name + '_id'];
                var result = _.findWhere(el, query);
                var index = el.indexOf(result);
                $localStorage[name][index] = data;
                resolve(data);
            }, 1500);
        });
    }

    obj.remove = function(name, id) {
        return new Promise(function(resolve, reject) {
            setTimeout(function() {
                var el = $localStorage[name];
                var query = {};
                query[name + '_id'] = id;
                var result = _.findWhere(el, query);
                el.splice(el.indexOf(result), 1)
                resolve();
            }, 1500);
        });
    }

    obj.load = function(name) {
        return $http.get(serviceBase + name + '.json');
    }

    return obj;
});
