var app = angular.module('myApp', [
        'ngRoute',
        'ngStorage',
        'ui-notification'
    ])
    .config(function(NotificationProvider) {
        NotificationProvider.setOptions({
            delay: 10000,
            startTop: 20,
            startRight: 10,
            verticalSpacing: 20,
            horizontalSpacing: 20,
            positionX: 'left',
            positionY: 'bottom'
        });
        // var $localStorage = $localStorageProvider.$get();
        //
        // $localStorage.myKey = 'value';
        // console.log($localStorage.myKey);
    });


app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider
            .when('/person', {
                templateUrl: 'app/person/person.html',
                controller: 'PersonCtrl'
            })
            .when('/user', {
                templateUrl: 'app/user/user.html',
                controller: 'UserCtrl'
            })
            .when('/position', {
                templateUrl: 'app/position/position.html',
                controller: 'PositionCtrl'
            })
            .when('/departament', {
                templateUrl: 'app/departament/departament.html',
                controller: 'DepartamentCtrl'
            })
            .when('/company', {
                templateUrl: 'app/company/company.html',
                controller: 'CompanyCtrl'
            })
            .when('/settings', {
                templateUrl: 'app/settings/settings.html',
                controller: 'SettingsCtrl'
            })
            .otherwise({
                redirectTo: '/settings'
            });
    }
]);


app.run(['$location', '$rootScope', function($location, $rootScope) {
    $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
        $rootScope.title = current.$$route.title;
    });
}]);
