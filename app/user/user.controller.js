app.controller('UserCtrl', function($scope, services, Notification) {
    var name = 'user';
    $scope.tableName = name;
    services.get(name, 'person', 'position')
        .then(function(data) {
            $scope.$apply(function() {
                _.extend($scope, data);
            });
        });

    $scope.add = function() {
        services.add(name, {})
            .then(function(result) {
                $scope.$apply(function() {
                    $scope.user.push(result);
                    Notification.success('Success');
                });
            })

    }

    $scope.remove = function(item) {
        services.remove(name, item.user_id)
            .then(function() {
                $scope.$apply(function() {
                    $scope.user = _.without($scope.user, item);
                    Notification.success('Success');
                });
            });
    }

    $scope.save = function(item) {
        services.update(name, item)
            .then(function(result) {
                $scope.$apply(function() {
                    item = result;
                    Notification.success('Success');
                });
            })
            .catch(function() {});
    }

});
