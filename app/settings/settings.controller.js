app.controller('SettingsCtrl', function($scope, services, $localStorage, Notification) {
    $scope.resetAll = function() {
        $localStorage.$reset();
        var prop = [
            services.load('user'),
            services.load('person'),
            services.load('company'),
            services.load('departament'),
            services.load('position')
        ]
        Promise.all(prop)
            .then(function(data) {
                $localStorage.$default({
                    user: data[0].data,
                    person: data[1].data,
                    company: data[2].data,
                    departament: data[3].data,
                    position: data[4].data,
                    counter: 1
                });
                  console.log($localStorage.company)
                Notification.success('Success');
            });
    }
    if ($localStorage.counter !== 1) {
      $scope.resetAll();
    }
});
