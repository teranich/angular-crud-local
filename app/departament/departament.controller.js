app.controller('DepartamentCtrl', function($scope, services, Notification) {
    var name = 'departament';
    $scope.tableName = name;
    services.get(name, 'company')
        .then(function(data) {
            $scope.$apply(function() {
                _.extend($scope, data);
            });
        });

    $scope.add = function() {
        services.add(name, {})
            .then(function(result) {
                $scope.$apply(function() {
                    $scope[name].push(result);
                    Notification.success('Success');
                });
            })

    }

    $scope.remove = function(item) {
        services.remove(name, item[name + '_id'])
            .then(function() {
                $scope.$apply(function() {
                    $scope[name] = _.without($scope[name], item);
                    Notification.success('Success');
                });
            });
    }

    $scope.save = function(item) {
        services.update(name, item)
            .then(function(result) {
                $scope.$apply(function() {
                    item = result;
                    Notification.success('Success');
                });
            })
            .catch(function() {});
    }

});
